# aioaws

Asyncio compatible SDK for aws services.

This library does not depend on boto, boto3 or any of the other bloated, opaque and mind thumbing AWS SDKs. Instead, it
is written from scratch to provide clean, secure and easily debuggable access to AWS services I want to use.


Forked from https://github.com/samuelcolvin/aioaws/commit/686bdf55f9b6e32cfa267349eb52f2cffd0fdab5.

Big thanks to Samuel Colvin and other original collaborators!





