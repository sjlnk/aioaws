{

  description = "aioaws python library";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    atsr-nix.url = "gitlab:atsresearch/lib_nix";
  };

  outputs = { self, flake-utils, ... }@inputs: let
    lib = inputs.nixpkgs.lib // builtins;
    atsr = inputs.atsr-nix.lib;
  in
  {

    overlays = {

      pythonSelector = (self: super: { python = super.python39; });

      pythonPackageOverrides = (self: super: {

        python = atsr.makePyPkgsOverride super.python (pypkgsSelf: pypkgsSuper: {

          aioaws = with pypkgsSelf; buildPythonPackage {
            name = "aioaws";
            version = "0.0.1";
            src = ./.;
            propagatedBuildInputs = [
              aiofiles
              cryptography
              httpx
              pydantic
              sortedcontainers
            ];
            doCheck = false;
          };

        });

      });

    };

  } // flake-utils.lib.eachDefaultSystem (system: let

      pkgs = import inputs.nixpkgs {
        inherit system;
        overlays = with self.overlays; [
          pythonSelector
          pythonPackageOverrides
        ];
      };

    in {

      defaultPackage = pkgs.python.pkgs.aioaws.overrideAttrs (old: {

        postShellHook = ''

          # This logic is normally done in sitecustomize.py, which comes with python nix package.
          # NIX_PYTHONPATH env var is used in that logic but unfortunately it's popped out of
          # the environment in sitecustomize.py and therefore is not passed on to the child
          # processes. Modifying sitecustomize.py would require modifying the python interpreter
          # which would invalidate all the python package caches. The other NIX_* env vars 
          # referred in sitecustomize.py are not used in this flake.
          # This function adds .pth file contents to PYTHONPATH as a workaround.
          prepend_pth_files_to_pythonpath() {

            local -a arr

            IFS=: read -a arr <<< "$PYTHONPATH"
            local sitepkgs
            local x
            for x in "''${arr[@]}"; do
              if [[ "$x" == $TMPDIR* ]]; then
                sitepkgs="$x"
                break
              fi
            done

            local fn
            for fn in $sitepkgs/*.pth; do
              readarray arr < $fn
              for x in "''${arr[@]}"; do
                x="''${x%%[[:space:]]}"  # strip trailing whitespace
                if [[ "$PYTHONPATH" != *$x* ]]; then
                  PYTHONPATH="$x''${PYTHONPATH:+:$PYTHONPATH}"
                fi
              done
            done

          }
          prepend_pth_files_to_pythonpath
          # there is no need to repeat this in sitecustomize.py, unsetting NIX_PYTHONPATH
          # disables this logic in sitecustomize.py
          unset NIX_PYTHONPATH

          prepend_pythonpath_bin_dirs_to_path() {

            local -a arr

            IFS=: read -a arr <<< "$PYTHONPATH"
            local x
            local bin_apdn
            for x in "''${arr[@]}"; do
              bin_apdn=$x/../../../bin
              [[ -d $bin_apdn ]] && PATH=$bin_apdn''${PATH:+:$PATH}
            done

          }
          prepend_pythonpath_bin_dirs_to_path

        '';
      });

    });

}
