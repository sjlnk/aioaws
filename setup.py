from setuptools import (
    setup,
    find_packages,
    find_namespace_packages,
)

# setup(
#     name='aioaws',
#     version="0.0.1",
#     packages=['aioaws'],
#     # python_requires='>=3.8',
#     # install_requires=[
#     #     'aiofiles>=0.5.0',
#     #     'cryptography>=3.1.1',
#     #     'httpx>=0.21.0',
#     #     'pydantic>=1.8.2',
#     # ],
# )

setup(name="aioaws",
      version="0.0.1",
      packages=["aioaws"],
      install_requires=[
          'aiofiles>=0.5.0',
          'cryptography>=3.1.1',
          'httpx>=0.21.0',
          'pydantic>=1.8.2',
          'sortedcontainers',
      ],
)

