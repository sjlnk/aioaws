import base64
import hashlib
import hmac
import logging
import asyncio
import httpx
from base64 import b64encode, b64decode
from binascii import hexlify
from datetime import datetime
from functools import reduce
from pprint import pprint, pformat
from sortedcontainers import SortedDict
from typing import TYPE_CHECKING, Any, Dict, List, Literal, Optional, Tuple
from urllib.parse import quote as url_quote

from httpx import URL, AsyncClient, Response

from ._utils import get_config_attr, utcnow, pretty_response
from .exceptions import RequestError

if TYPE_CHECKING:
    from ._types import BaseConfigProtocol

L = logging.getLogger('aioaws.core')

_AWS_AUTH_REQUEST = 'aws4_request'
_CONTENT_TYPE = 'application/x-www-form-urlencoded'
_AUTH_ALGORITHM = 'AWS4-HMAC-SHA256'


class AwsClient:
    """
    HTTP client for AWS with authentication
    """

    def __init__(self, client: AsyncClient, config: 'BaseConfigProtocol', service: Literal['s3', 'ses']):
        self.client = client
        self.aws_access_key = get_config_attr(config, 'aws_access_key')
        self.aws_secret_key = get_config_attr(config, 'aws_secret_key')
        self.service = service
        self.region = get_config_attr(config, 'aws_region')
        if self.service == 'ses':
            self.host = f'email.{self.region}.amazonaws.com'
        else:
            assert self.service == 's3', self.service
            bucket = get_config_attr(config, 'aws_s3_bucket')
            if bucket:
                if '.' in bucket:
                    # assumes the bucket is a domain and is already as a CNAME record for S3
                    self.host = bucket
                else:
                    # see https://docs.aws.amazon.com/AmazonS3/latest/userguide/access-bucket-intro.html
                    self.host = f'{bucket}.s3.{self.region}.amazonaws.com'
                self.endpoint = f'https://{self.host}'
            else:
                # bucket is not required for all s3 api methods
                self.host = None
                self.endpoint = None


    # async def get(self, path: str = '', *, params: Optional[Dict[str, Any]] = None) -> Response:
    #     return await self.request('GET', path=path, params=params)

    # async def raw_post(
    #     self,
    #     url: str,
    #     *,
    #     expected_status: int,
    #     params: Optional[Dict[str, Any]] = None,
    #     data: Optional[Dict[str, str]] = None,
    #     files: Optional[Dict[str, bytes]] = None,
    # ) -> Response:
    #     r = await self.client.post(url, params=params, data=data, files=files)
    #     if r.status_code == expected_status:
    #         return r
    #     else:
    #         # from ._utils import pretty_response
    #         # pretty_response(r)
    #         raise RequestError(r)

    # async def post(
    #     self,
    #     path: str = '',
    #     *,
    #     params: Optional[Dict[str, Any]] = None,
    #     data: Optional[bytes] = None,
    #     content_type: Optional[str] = None,
    # ) -> Response:
    #     return await self.request('POST', path=path, params=params, data=data, content_type=content_type)


    def _make_request_dict(
        self,
        method,
        *,
        path=None,
        params: Optional[Dict[str, Any]] = None,
        data: Optional[bytes] = None,
        headers=None,
        endpoint=None,
        host=None,
        data_checksum_override=None,
    ):
        endpoint = endpoint or self.endpoint
        host = host or self.host
        if not path:
            path = ""
        else:
            path = f'/{path}'
        url = URL(f'{endpoint}{path}', params=[(k, v) for k, v in sorted((params or {}).items())])
        headers = headers or {}
        headers["host"] = host
        extra_headers = self._make_auth_headers(
            method,
            url, 
            data=data,
            headers=headers, 
            data_checksum_override=data_checksum_override,
        )
        headers.update(extra_headers)
        # pprint(headers)
        res = {
            "method": method,
            "url": url,
            "content": data,
            "headers": headers
        }
        return res


    async def request(self, *args, **kwargs):
        retry = kwargs.pop("retry", False)
        req_dict = self._make_request_dict(*args, **kwargs)
        while True:
            L.debug(f'making a request: method={req_dict["method"]}, '
                    f'url={req_dict["url"]} ...')
            try:
                r = await self.client.request(**req_dict)
            except (httpx.ReadTimeout, httpx.WriteTimeout,
                    httpx.ReadError, httpx.WriteError) as err:
                if not retry:
                    raise
                L.debug(f'{err.__class__.__name__} on request: '
                        f'method={req_dict["method"]}, '
                        f'url={req_dict["url"]} ...')
                await asyncio.sleep(5)
            else:
                # pretty_response(r)
                if r.is_success:
                    return r
                raise RequestError(r)


    def stream(self, *args, **kwargs):
        req_dict = self._make_request_dict(*args, **kwargs)
        L.debug(f'streaming a request: method={req_dict["method"]}, '
                f'url={req_dict["url"]} ...')
        return self.client.stream(**req_dict)

    # def add_signed_download_params(self, method: Literal['GET', 'POST'], url: URL, expires: int = 86400) -> URL:
    #     assert expires >= 1, f'expires must be greater than or equal to 1, not {expires}'
    #     assert expires <= 604800, f'expires must be less than or equal to 604800, not {expires}'
    #     now = utcnow()
    #     url = url.copy_merge_params(
    #         {
    #             'X-Amz-Algorithm': _AUTH_ALGORITHM,
    #             'X-Amz-Credential': self._aws4_credential(now),
    #             'X-Amz-Date': _aws4_x_amz_date(now),
    #             'X-Amz-Expires': str(expires),
    #             'X-Amz-SignedHeaders': 'host',
    #         }
    #     )
    #     _, signature = self._aws4_signature(now, method, url, {'host': self.host}, 'UNSIGNED-PAYLOAD')
    #     return url.copy_add_param('X-Amz-Signature', signature)

    def upload_extra_conditions(self, dt: datetime) -> List[Dict[str, str]]:
        return [
            {'x-amz-credential': self._aws4_credential(dt)},
            {'x-amz-algorithm': _AUTH_ALGORITHM},
            {'x-amz-date': _aws4_x_amz_date(dt)},
        ]

    def signed_upload_fields(self, dt: datetime, string_to_sign: str) -> Dict[str, str]:
        return {
            'X-Amz-Algorithm': _AUTH_ALGORITHM,
            'X-Amz-Credential': self._aws4_credential(dt),
            'X-Amz-Date': _aws4_x_amz_date(dt),
            'X-Amz-Signature': self._aws4_sign_string(string_to_sign, dt),
        }

    
    @staticmethod
    def _is_canonical_header(header):
        if header.startswith("x-amz"):
            return True
        if header in { "content-md5", "content-type", "host" }:
            return True
        return False


    def _make_auth_headers(
        self,
        method,
        url,
        headers,
        data_checksum_override,
        data,
    ):

        now = utcnow()
        data = data or b''
        content_sha256_hash = hashlib.sha256(data).digest()
        content_csum = hexlify(content_sha256_hash).decode()
        if data_checksum_override:
            data_csum = data_checksum_override
        else:
            data_csum = b64encode(content_sha256_hash).decode()

        # canonical headers need to be in alphabetical order
        canonical_headers = SortedDict(
            {k.lower(): v for k, v in headers.items() 
             if self._is_canonical_header(k)}
        )
        canonical_headers["x-amz-date"] = _aws4_x_amz_date(now)
        canonical_headers["x-amz-checksum-sha256"] = data_csum

        signed_headers, signature = self._aws4_signature(
                now, method, url, canonical_headers, content_csum)
        credential = self._aws4_credential(now)
        authorization_header = (
            f'{_AUTH_ALGORITHM} Credential={credential},'
            f'SignedHeaders={signed_headers},Signature={signature}'
        )
        res = {
            'authorization': authorization_header, 
            'x-amz-content-sha256': content_csum,
            # adding payload hash for server side data integrity check
            'x-amz-checksum-sha256': data_csum,
            'x-amz-date': canonical_headers["x-amz-date"]
        }
        return res

    def _aws4_signature(
        self, dt: datetime, method: Literal['GET', 'POST'], url: URL, headers: Dict[str, str], payload_hash: str
    ) -> Tuple[str, str]:
        header_keys = sorted(headers)
        signed_headers = ';'.join(header_keys)
        canonical_request_parts = (
            method,
            url_quote(url.path),
            url.query.decode(),
            ''.join(f'{k}:{headers[k]}\n' for k in header_keys),
            signed_headers,
            payload_hash,
        )
        canonical_request = '\n'.join(canonical_request_parts)
        string_to_sign_parts = (
            _AUTH_ALGORITHM,
            _aws4_x_amz_date(dt),
            self._aws4_scope(dt),
            hashlib.sha256(canonical_request.encode()).hexdigest(),
        )
        string_to_sign = '\n'.join(string_to_sign_parts)
        return signed_headers, self._aws4_sign_string(string_to_sign, dt)

    def _aws4_sign_string(self, string_to_sign: str, dt: datetime) -> str:
        key_parts = (
            b'AWS4' + self.aws_secret_key.encode(),
            _aws4_date_stamp(dt),
            self.region,
            self.service,
            _AWS_AUTH_REQUEST,
            string_to_sign,
        )
        signature_bytes: bytes = reduce(_aws4_reduce_signature, key_parts)  # type: ignore
        return hexlify(signature_bytes).decode()

    def _aws4_scope(self, dt: datetime) -> str:
        return f'{_aws4_date_stamp(dt)}/{self.region}/{self.service}/{_AWS_AUTH_REQUEST}'

    def _aws4_credential(self, dt: datetime) -> str:
        return f'{self.aws_access_key}/{self._aws4_scope(dt)}'


def _aws4_date_stamp(dt: datetime) -> str:
    return dt.strftime('%Y%m%d')


def _aws4_x_amz_date(dt: datetime) -> str:
    return dt.strftime('%Y%m%dT%H%M%SZ')


def _aws4_reduce_signature(key: bytes, msg: str) -> bytes:
    return hmac.new(key, msg.encode(), hashlib.sha256).digest()


