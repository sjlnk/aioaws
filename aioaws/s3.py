import asyncio
import json
import hashlib
import httpx
import mimetypes
import io
import itertools
import re
import logging
from pathlib import Path
from base64 import b64encode, b64decode
from pprint import pprint
from copy import deepcopy
from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import TYPE_CHECKING, Any, AsyncIterable, Dict, List, Optional, Union
from xml.etree import ElementTree as ET
from contextlib import asynccontextmanager, suppress
from httpx import URL, AsyncClient
from pydantic import ( 
    BaseModel as PydanticBaseModel,
    validator,
)
from ._utils import ManyTasks, utcnow, pretty_response
from .core import AwsClient
from .exceptions import RequestError

if TYPE_CHECKING:
    from ._types import S3ConfigProtocol

__all__ = 'S3Client', 'S3Config', 'S3Object'

L = logging.getLogger('aioaws.s3')

# rounding of download link expiry time, this allows the CDN to efficiently cache download links
EXPIRY_ROUNDING = 100
# removing xmlns="http://s3.amazonaws.com/doc/2006-03-01/" from xml makes it much easier to parse
XMLNS = 'http://s3.amazonaws.com/doc/2006-03-01/'
XMLNS_RE = re.compile(f' xmlns="{re.escape(XMLNS)}"'.encode())


def xml_bool_to_py_bool(x):
    if x == "true":
        return True
    if x == "false":
        return False
    raise ValueError(f'invalid xml bool: {x}')


def xml_elem_to_dict(el, parsers):
    res = {}
    for x in el:
        if (parser := parsers.get(x.tag)):
            res[x.tag] = parser(x.text)
        else:
            res[x.tag] = x.text
    return res


def et_dump(el):
    el = deepcopy(el)
    ET.indent(el, "  ")
    ET.dump(el)


@dataclass
class S3Config:

    aws_access_key: str
    aws_secret_key: str
    aws_region: str
    aws_s3_bucket: str


class BaseModel(PydanticBaseModel):
    class Config:
        allow_population_by_field_name = True
        @classmethod
        def alias_generator(cls, string: str) -> str:
            return ''.join(word.capitalize() for word in string.split('_'))


class S3Owner(BaseModel):

    display_name: Optional[str]
    id: Optional[str]

    @classmethod
    def parse_obj(cls, x):
        x = deepcopy(x)
        if "id" not in x:
            x["id"] = x.pop("ID", None)
        return super().parse_obj(x)


class S3Initiator(S3Owner):
    pass


class S3CompletedPart(BaseModel):

    checksum_crc32: Optional[str]
    checksum_crc32c: Optional[str]
    checksum_sha1: Optional[str]
    checksum_sha256: Optional[str]
    e_tag: Optional[str]
    part_number: Optional[int]

    @classmethod
    def parse_obj(cls, x):
        x = deepcopy(x)
        if "checksum_crc32" not in x:
            x["checksum_crc32"] = x.pop("ChecksumCRC32", None)
        if "checksum_crc32c" not in x:
            x["checksum_crc32c"] = x.pop("ChecksumCRC32C", None)
        if "checksum_sha1" not in x:
            x["checksum_sha1"] = x.pop("ChecksumSHA1", None)
        if "checksum_sha256" not in x:
            x["checksum_sha256"] = x.pop("ChecksumSHA256", None)
        return super().parse_obj(x)


    # @validator('e_tag')
    # def set_ts_now(cls, v: str) -> str:
    #     return v.strip('"')


class S3Part(S3CompletedPart):

    last_modified: Optional[datetime]
    size: Optional[int]


class S3Object(BaseModel):

    checksum_algorithm: Optional[str]
    e_tag: Optional[str]
    key: Optional[str]
    owner: Optional[S3Owner]
    last_modified: Optional[datetime]
    size: Optional[int]
    storage_class: Optional[str]


    # @validator('e_tag')
    # def set_ts_now(cls, v: str) -> str:
    #     return v.strip('"')


class S3ObjectVersion(S3Object):

    is_latest: Optional[bool]
    version_id: Optional[str]


class S3MultipartUpload(BaseModel):

    checksum_algorithm: Optional[str]
    initiated: Optional[datetime]
    initiator: Optional[S3Initiator]
    key: Optional[str]
    owner: Optional[S3Owner]
    storage_class: Optional[str]
    upload_id: Optional[str]


class S3DeletedObject(BaseModel):

    delete_marker: Optional[bool]
    delete_marker_version_id: Optional[str]
    key: Optional[str]
    version_id: Optional[str]


class S3DeleteMarkerEntry(BaseModel):

    is_latest: Optional[bool]
    key: Optional[str]
    last_modified: Optional[datetime]
    version_id: Optional[str]


class S3Error(BaseModel):
    
    code: Optional[str]
    key: Optional[str]
    message: Optional[str]
    version_id: Optional[str]


class S3Transfer:


    def __init__(self, s3_client):
        self._client = s3_client

    
    @staticmethod
    def _extract_bytes(x):
        if isinstance(x, Path):
            with data_spec.open("rb") as f:
                return f.read()
        elif hasattr(x, "read"):
            data = data_spec.read()
            if not isinstance(data, bytes):
                data = data.encode()
            return data
        if not isinstance(x, bytes):
            raise ValueError(f'cannot extract bytes from type: "{type(x)}"')
        return x


    async def upload(
        self, 
        key,
        data_spec,
        *,
        content_type=None,
        storage_class="STANDARD",
        part_size=5*1024**2,
        upload_id=None,
        concurrency=5,
    ):
        if isinstance(data_spec, (list, dict, tuple, set)):
            chunks = (self._extract_bytes(x) for x in data_spec)
        else:
            if isinstance(data_spec, Path):
                f = data_spec.open("rb")
            elif hasattr(data_spec, "read"):
                f = data_spec
            else:
                f = io.BytesIO(data_spec)
            def get_chunks():
                while (chunk := f.read(part_size)):
                    # print(len(chunk))
                    yield chunk
            chunks = get_chunks()

        first_two = list(itertools.islice(chunks, 0, 2)) 
        # remaining_chunks = itertools.islice(chunks, 2, None)

        # async def with_retry(action_label, aw):
        #     while True:
        #         try:
        #             return await aw
        #         except TransportError as err:
        #             if not retry:
        #                 raise
        #             L.exception(f'error on {action_label}:')
        #             L.info(f'retrying in {retry_secs} seconds ...')
        #         await asyncio.sleep(retry_secs)

        if len(first_two) == 1:
            await self._client.put_object(
                key=key,
                data=first_two[0],
                content_type=content_type,
                storage_class=storage_class,
            )
            yd = {
                "msg_type": "single",
                # "data": res,
                "parts_uploaded": 1,
                "total_parts_uploaded": 1, 
                "bytes_uploaded": len(first_two[0]),
                "total_bytes_uploaded": len(first_two[0]),
            }
            yield yd
            return
        parts = []
        if upload_id:
            try:
                parts = await self._client.list_parts(key, upload_id)
            except RequestError as err:
                L.debug(f'error ({err.status}) on list_parts request, '
                        f'creating a new upload ...')
        if not parts:
            upload_id = await self._client.create_multipart_upload(
                key=key,
                content_type=content_type,
                storage_class=storage_class
            )

        chunks = itertools.chain(first_two, chunks)
        parts = {v.part_number: v for v in parts}

        yd = {
            "msg_type": "multipart",
            "data": upload_id,
            "parts_uploaded": 0,
            "total_parts_uploaded": 0, 
            "bytes_uploaded": 0,
            "total_bytes_uploaded": 0,
        }
        yield yd
        
        async def upload_part(part_number, data):
            # print(f"uploading {part_number}")
            res = await self._client.upload_part(
                    key=key,
                    upload_id=upload_id,
                    part_number=part_number,
                    data=data)
            return res, len(data)

        tasks = set()
        i = 0
        bytes_uploaded = 0
        total_bytes_uploaded = 0
        parts_uploaded = 0
        total_parts_uploaded = 0 
        iter_exhausted = False

        while not (iter_exhausted and total_parts_uploaded == i):

            if len(tasks) < concurrency:
                try: 
                    chunk = next(chunks)
                    part_number = i + 1
                    part = parts.get(part_number)
                    part_already_uploaded = False
                    if part:
                        part_csum = b64encode(hashlib.sha256(chunk).digest()).decode()
                        if part_csum == part.checksum_sha256:
                            part_already_uploaded = True
                        else:
                            del parts[part_number]
                    if part_already_uploaded:
                        # print(f'part {part_number} already uploaded, skipping ...')
                        total_parts_uploaded += 1
                        total_bytes_uploaded += part.size
                    else:
                        tasks.add(asyncio.create_task(
                            upload_part(part_number, chunk), name=str(part_number)))
                    i += 1
                except StopIteration:
                    iter_exhausted = True
            
            for task in set(tasks):
                if task.done():
                    tasks.remove(task)
                    res, num_bytes = await task
                    part = deepcopy(res)
                    parts[part.part_number] = part
                    bytes_uploaded += num_bytes
                    total_bytes_uploaded += num_bytes
                    parts_uploaded += 1
                    total_parts_uploaded += 1
                    # print(f'upload task {task.get_name()} finished')
                    yd = {
                        "msg_type": "part",
                        "data": res,
                        "parts_uploaded": parts_uploaded,
                        "total_parts_uploaded": total_parts_uploaded,
                        "bytes_uploaded": bytes_uploaded,
                        "total_bytes_uploaded": total_bytes_uploaded,
                    }
                    yield yd
            
            if iter_exhausted or len(tasks) >= concurrency:
                await asyncio.sleep(0.1)
    
        parts = sorted(parts.values(), key=lambda x: x.part_number)
        # remove trailing parts from previous upload, if any ...
        parts = parts[:total_parts_uploaded]
        res = await self._client.complete_multipart_upload(
            key=key,
            upload_id=upload_id,
            parts=parts
        )
        yd = {
            "msg_type": "complete",
            "data": res,
            "parts_uploaded": parts_uploaded,
            "total_parts_uploaded": total_parts_uploaded,
            "bytes_uploaded": bytes_uploaded,
            "total_bytes_uploaded": total_bytes_uploaded,
        }
        yield yd
        

class S3Client:

    __slots__ = '_config', '_aws_client', 'retry'


    def __init__(
        self,
        http_client: AsyncClient,
        config: 'S3ConfigProtocol',
        retry: bool = True,
    ):
        self._aws_client = AwsClient(http_client, config, 's3')
        self._config = config
        self.retry = retry


    async def list_objects(
        self,
        prefix: Optional[str] = None,
        delimiter: Optional[str] = None,
    ):

        continuation_token = None
        objects = []
        prefixes = []

        while True:

            # TODO: support for fetch-owner, start-after/singlepage
            params = {
                'list-type': 2,
                'prefix': prefix,
                'continuation-token': continuation_token,
            }
            if delimiter:
                params["delimiter"] = delimiter
                        
            r = await self._aws_client.request(
                method="GET",
                params={k: v for k, v in params.items() if v is not None},
                retry=self.retry,
            )

            xml_root = ET.fromstring(XMLNS_RE.sub(b'', r.content))
            # et_dump(xml_root)
            
            for el in xml_root.findall('Contents'):
                d = {x.tag: x for x in el}
                for k, v in d.items():
                    if k == "Owner":
                        d[k] = {x.tag: x.text for x in v}
                    else:
                        d[k] = v.text
                objects.append(S3Object.parse_obj(d))
                
            for el in xml_root.findall('CommonPrefixes'):
                prefixes.append(el.find('Prefix').text)
                
            if (t := xml_root.find('IsTruncated')) is not None and t.text == 'false':
                break

            if (t := xml_root.find('NextContinuationToken')) is not None:
                continuation_token = t.text
            else:
                raise RuntimeError(f'unexpected response from S3: {r.text!r}')

        return objects, prefixes


    async def list_object_versions(
        self,
        prefix: Optional[str] = None,
        delimiter: Optional[str] = None,
    ):

        versions = []
        prefixes = []

        while True:

            # TODO: support for fetch-owner and pagination
            params = {
                'versions': True,
                'prefix': prefix,
            }
            if delimiter:
                params["delimiter"] = delimiter
                        
            r = await self._aws_client.request(
                method="GET",
                params={k: v for k, v in params.items() if v is not None},
                retry=self.retry,
            )

            xml_root = ET.fromstring(XMLNS_RE.sub(b'', r.content))
            # et_dump(xml_root)
            
            for el in xml_root.findall('Version'):
                d = {x.tag: x for x in el}
                for k, v in d.items():
                    if k == "Owner":
                        d[k] = {x.tag: x.text for x in v}
                    else:
                        d[k] = v.text
                versions.append(S3ObjectVersion.parse_obj(d))
                
            for el in xml_root.findall('DeleteMarker'):
                versions.append(S3DeleteMarkerEntry.parse_obj({v.tag: v.text for v in el}))
                
            for el in xml_root.findall('CommonPrefixes'):
                prefixes.append(el.find('Prefix').text)

            if (t := xml_root.find('IsTruncated')) is not None and t.text == 'false':
                break
            else:
                raise NotImplementedError("pagination is not implemented")

            # if (t := xml_root.find('NextContinuationToken')) is not None:
            #     continuation_token = t.text
            # else:
            #     raise RuntimeError(f'unexpected response from S3: {r.text!r}')

        versions = sorted(versions, key=lambda x: (x.key, x.last_modified))
        return versions, prefixes


    async def list_multipart_uploads(
        self,
        prefix: Optional[str] = None,
        delimiter: Optional[str] = None,
    ):

        objects = []
        prefixes = []

        while True:

            # TODO: support for pagination

            params = {
                "uploads": True,
                "prefix": prefix,
            }
            if delimiter:
                params["delimiter"] = delimiter
                        
            r = await self._aws_client.request(
                method="GET",
                params={k: v for k, v in params.items() if v is not None},
                retry=self.retry,
            )

            xml_root = ET.fromstring(XMLNS_RE.sub(b'', r.content))
            # et_dump(xml_root)
            
            for el in xml_root.findall("Upload"):
                d = {x.tag: x for x in el}
                for k, v in d.items():
                    if k in {"Owner", "Initiator"}:
                        d[k] = {x.tag: x.text for x in v}
                    else:
                        d[k] = v.text
                objects.append(S3MultipartUpload.parse_obj(d))
                
            for el in xml_root.findall("CommonPrefixes"):
                prefixes.append(el.find("Prefix").text)
                
            if (t := xml_root.find("IsTruncated")) is not None and t.text == "false":
                break

            raise NotImplementedError()
        return objects, prefixes


    async def list_parts(self, key, upload_id):
        
        objects = []

        params = { "uploadId": upload_id }

        while True:

            r = await self._aws_client.request(
                method="GET",
                path=key,
                params={k: v for k, v in params.items() if v is not None},
                retry=self.retry,
            )

            xml_root = ET.fromstring(XMLNS_RE.sub(b'', r.content))
            # et_dump(xml_root)
            
            for el in xml_root.findall("Part"):
                objects.append(S3Part.parse_obj({v.tag: v.text for v in el}))
                
            if (t := xml_root.find("IsTruncated")) is not None and t.text == "false":
                break

            params["part-number-marker"] = xml_root.find("NextPartNumberMarker").text

        return objects


    async def delete_objects(self, objects, verbose=False):
        tasks = ManyTasks()
        chunk_size = 1000
        for i in range(0, len(objects), chunk_size):
            coro = self._delete_1000_objects(
                    objects[i:i+chunk_size], verbose=verbose)
            tasks.add(coro)
        results = await tasks.finish()
        return list(itertools.chain(*results))


    async def put_object(
        self, 
        key,
        data,
        *,
        content_type=None,
        storage_class="STANDARD",
    ) -> None:
        
        # TODO: add support for tags

        if content_type is None:
            content_type, _ = mimetypes.guess_type(key)

        headers = {}
        if content_type:
            headers["content-type"] = content_type
        headers["x-amz-storage-class"] = storage_class

        res = await self._aws_client.request(
            method="PUT",
            path=key,
            data=data,
            headers=headers,
            retry=self.retry,
        )
        # pretty_response(res)


    async def upload_part(self, key, upload_id, part_number, data):
        # minimum size for a part that is not the last part is 5 MiB
        params = {
            "uploadId": upload_id,
            "partNumber": part_number,
        }
        r = await self._aws_client.request(
            method="PUT",
            path=key,
            data=data,
            params=params,
            retry=self.retry,
        )
        d = {}
        d["part_number"] = part_number
        d["e_tag"] = r.headers["ETag"]
        if (checksum := r.headers.get("x-amz-checksum-crc32")):
            d["checksum_crc32"] = checksum
        if (checksum := r.headers.get("x-amz-checksum-crc32c")):
            d["checksum_crc32c"] = checksum
        if (checksum := r.headers.get("x-amz-checksum-sha1")):
            d["checksum_sha1"] = checksum
        if (checksum := r.headers.get("x-amz-checksum-sha256")):
            d["checksum_sha256"] = checksum
        res = S3CompletedPart.parse_obj(d)
        return res


    async def upload_part_copy():
        # very similar to copy_object ... implement later ...
        raise NotImplementedError()
        

    async def create_multipart_upload(
        self, 
        key,
        *,
        content_type=None,
        storage_class="STANDARD",
    ):

        # TODO: add support for tags

        headers = {}
        params = {}
        params["uploads"] = True
        
        if content_type:
            headers["content-type"] = content_type
        headers["x-amz-storage-class"] = storage_class
        headers["x-amz-checksum-algorithm"] = "SHA256"

        r = await self._aws_client.request(
            method="POST",
            path=key,
            headers=headers,
            params=params,
            retry=self.retry,
        )

        xml_root = ET.fromstring(XMLNS_RE.sub(b'', r.content))
        # et_dump(xml_root)

        return xml_root.find("UploadId").text


    async def complete_multipart_upload(self, key, upload_id, parts):
        params = {"uploadId": upload_id}

        checksum_bytes = b""
        root = ET.Element("CompleteMultipartUpload", xmlns=XMLNS)
        for x in parts:
            obj = ET.SubElement(root, "Part")
            if x.checksum_sha256:
                checksum_bytes += b64decode(x.checksum_sha256)
                ET.SubElement(obj, "ChecksumSHA256").text = x.checksum_sha256
            ET.SubElement(obj, "ETag").text = x.e_tag
            ET.SubElement(obj, "PartNumber").text = str(x.part_number)

        data_checksum_override = hashlib.sha256(checksum_bytes).digest()
        data_checksum_override = b64encode(data_checksum_override).decode()
        data_checksum_override += f'-{len(parts)}'

        xml = ET.tostring(root, encoding="unicode", xml_declaration=True)

        r = await self._aws_client.request(
            method="POST",
            path=key,
            params=params,
            data=xml.encode(),
            data_checksum_override=data_checksum_override,
            retry=self.retry,
        )
        xml_root = ET.fromstring(XMLNS_RE.sub(b'', r.content))
        # et_dump(xml_root)

        res = {}
        res["e_tag"] = xml_root.find("ETag").text
        if (el := xml_root.find("ChecksumCRC32")) is not None:
            res["checksum_crc32"] = el.text
        if (el := xml_root.find("ChecksumCRC32C")) is not None:
            res["checksum_crc32c"] = el.text
        if (el := xml_root.find("ChecksumSHA1")) is not None:
            res["checksum_sha1"] = el.text
        if (el := xml_root.find("ChecksumSHA256")) is not None:
            res["checksum_sha256"] = el.text
        return res
        

    async def abort_multipart_upload(self, key, upload_id):
        params = {"uploadId": upload_id}
        r = await self._aws_client.request(
            method="DELETE",
            path=key,
            params=params,
            retry=self.retry,
        )
        # pretty_response(r)
        

    async def restore_object(
        self,
        obj_spec,
        days_to_keep,
        *,
        version_id=None,
        restoration_speed="Standard",
    ):

        # TODO: implement support for select
        
        if type(obj_spec) == str:
            key = obj_spec
        else:
            key = obj_spec.key
            version_id = obj_spec.version_id
        params = {}
        params["restore"] = True
        if version_id:
            params["versionId"] = version_id

        root = ET.Element("RestoreRequest", xmlns=XMLNS)
        ET.SubElement(root, "Days").text = str(days_to_keep)
        el = ET.SubElement(root, "GlacierJobParameters") 
        ET.SubElement(el, "Tier").text = restoration_speed
        xml = ET.tostring(root, encoding="unicode", xml_declaration=True)

        res = await self._aws_client.request(
            method="POST",
            path=key,
            data=xml.encode(),
            params=params,
            retry=self.retry,
        )

        # pretty_response(res)

        if res.status_code == 200:
            # restored object already exists
            return 

        if res.status_code == 202:
            # restoration process started
            return dict(res.headers)


    async def copy_object(
        self,
        src_spec,
        *,
        keep_metadata=False,
        dest_key=None,
        version_id=None,
        content_type=None,
        storage_class="STANDARD",
    ):

        # TODO: add support for conditionals 

        headers = {}
        params = {}
    
        if keep_metadata:
            headers["x-amz-metadata-directive"] = "COPY"
        else:
            headers["x-amz-metadata-directive"] = "REPLACE"
            headers["x-amz-storage-class"] = storage_class
            if content_type:
                headers["content-type"] = content_type
                
        if type(src_spec) == str:
            src = src_spec
        else:
            src = f'{self._config.aws_s3_bucket}/{src_spec.key}'
            version_id = src_spec.version_id
        if version_id:
            src += f'?versionId={version_id}'
        headers["x-amz-copy-source"] = src

        if dest_key:
            key = dest_key
        else:
            if src.startswith("arn:aws:s3"):
                raise NotImplementedError(
                        "inferring key from arn url is not yet implemented")
            else:
                key = src.partition("/")[2]

        headers["x-amz-checksum-algorithm"] = "SHA256"

        r = await self._aws_client.request(
            method="PUT",
            path=key,
            params=params,
            headers=headers,
            retry=self.retry,
        )
        res = dict(r.headers)
        return res


    def _get_object_req_dict(
        self,
        obj_spec,
        *,
        part_number=None,
        byte_range=None,
        version_id=None,
        header_only=False,
        streaming=False,
    ):
        # TODO: add support for conditions

        if type(obj_spec) == str:
            key = obj_spec
        else:
            key = obj_spec.key
            version_id = obj_spec.version_id
        params = {}
        headers = {}
        if version_id:
            params["versionId"] = version_id
        if part_number:
            params["partNumber"] = part_number
        if byte_range:
            headers["range"] = f'bytes={byte_range[0]}-{byte_range[1]}'
        # it is always good to return the checksum as well ...
        headers["x-amz-checksum-mode"] = "ENABLED"
        req_dict = {
            "method": "HEAD" if header_only else "GET",
            "path": key,
            "params": params,
            "headers": headers,
        }
        return req_dict



    async def get_object(
        self,
        obj_spec,
        *,
        part_number=None,
        byte_range=None,
        version_id=None,
        header_only=False,
    ):
        req_dict = self._get_object_req_dict(
            obj_spec=obj_spec,
            part_number=part_number,
            byte_range=byte_range,
            version_id=version_id,
            header_only=header_only)
        r = await self._aws_client.request(
            **req_dict,
            retry=self.retry,
        )
        res = dict(r.headers)
        if not header_only:
            res["content"] = r.content
        return res


    def stream_object(
        self,
        obj_spec,
        *,
        part_number=None,
        byte_range=None,
        version_id=None,
    ):
        req_dict = self._get_object_req_dict(
            obj_spec=obj_spec,
            part_number=part_number,
            byte_range=byte_range,
            version_id=version_id,
            header_only=False,
        )
        return self._aws_client.stream(**req_dict)


    async def get_object_attributes(
        self,
        obj_spec,
        *,
        version_id=None,
    ):

        # TODO: add support for conditions

        attributes = ",".join([
            "ETag",
            "Checksum", 
            "ObjectParts", 
            "StorageClass",
            "ObjectSize"
        ])

        if type(obj_spec) == str:
            key = obj_spec
        else:
            key = obj_spec.key
            version_id = obj_spec.version_id

        res = {}
        res["parts"] = parts = []

        while True:
        
            params = {}
            headers = {}

            if version_id:
                params["versionId"] = version_id
            params["attributes"] = True
            
            headers["x-amz-object-attributes"] = attributes

            r = await self._aws_client.request(
                method="GET",
                path=key,
                params=params,
                headers=headers,
                retry=self.retry,
            )
        
            xml_root = ET.fromstring(XMLNS_RE.sub(b'', r.content))
            # et_dump(xml_root)
            
            res["e_tag"] = xml_root.find("ETag").text
            res["storage_class"] = xml_root.find("StorageClass").text
            res["size"] = int(xml_root.find("ObjectSize").text)

            csum = xml_root.find("Checksum")
            if (el := csum.find("ChecksumCRC32")) is not None:
                res["checksum_crc32"] = el.text
            if (el := csum.find("ChecksumCRC32C")) is not None:
                res["checksum_crc32c"] = el.text
            if (el := csum.find("ChecksumSHA1")) is not None:
                res["checksum_sha1"] = el.text
            if (el := csum.find("ChecksumSHA256")) is not None:
                res["checksum_sha256"] = el.text

            object_parts = xml_root.find("ObjectParts")
            if object_parts:
                for el in object_parts.findall("Part"):
                    part = S3Part.parse_obj({v.tag: v.text for v in el})
                    part = {k: v for k, v in part.dict().items() 
                            if v is not None}
                    parts.append(part)
                if (
                    (t := object_parts.find("IsTruncated")) is not None and \
                    t.text == "false"
                ):
                    break
            else:
                break

            raise NotImplementedError("pagination not implemented")

        return res


    async def truncate_versions(
        self,
        key,
        keep,
        verbose=False,
        delete_markers_only=False,
    ):
        versions, _ = await self.list_object_versions(prefix=key)
        versions = [x for x in versions if x.key == key]
        versions = sorted(versions, key=lambda x: x.last_modified)[::-1]
        if delete_markers_only:
            versions_to_del = [x for x in versions[1:] 
                               if type(x) == S3DeleteMarkerEntry]
        else:
            versions_to_del = versions[keep:]
        if keep > 0:
            # sanity check: make sure latest versions are not deleted
            assert not any([x.is_latest for x in versions_to_del])
        if not versions_to_del:
            return []
        return await self.delete_objects(versions_to_del, verbose=verbose)


    async def purge_old_delete_markers(self, key, verbose=False):
        return await self.truncate_versions(
                key=key, keep=0, 
                verbose=verbose, delete_markers_only=True)


    # async def upload(self, file_path: str, content: bytes, *, content_type: Optional[str] = None) -> None:
    #     assert not file_path.startswith('/'), 'file_path must not start with /'
    #     parts = file_path.rsplit('/', 1)

    #     if content_type is None:
    #         content_type, _ = mimetypes.guess_type(file_path)

    #     d = self.signed_upload_url(
    #         path=f'{parts[0]}/' if len(parts) > 1 else '',
    #         filename=parts[-1],
    #         content_type=content_type or 'application/octet-stream',
    #         size=len(content),
    #         expires=datetime.utcnow() + timedelta(minutes=30),
    #     )
    #     await self._aws_client.raw_post(d['url'], expected_status=204, data=d['fields'], files={'file': content})


    # async def delete_recursive(self, prefix: Optional[str]) -> List[str]:
    #     """
    #     Delete files starting with a specific prefix.
    #     """
    #     files = []
    #     tasks = ManyTasks()
    #     async for f in self.list_objects(prefix):
    #         files.append(f)
    #         if len(files) == 1000:
    #             tasks.add(self._delete_1000_files(*files))
    #             files = []

    #     if files:
    #         tasks.add(self._delete_1000_files(*files))
    #     results = await tasks.finish()
    #     return list(chain(*results))


    async def _delete_1000_objects(self, objects, verbose):
        """
        https://docs.aws.amazon.com/AmazonS3/latest/API/API_DeleteObjects.html
        """
        assert len(objects) <= 1000
        root = ET.Element("Delete", xmlns=XMLNS)
        for x in objects:
            obj = ET.SubElement(root, "Object")
            if hasattr(x, "key") and x.key:
                ET.SubElement(obj, "Key").text = x.key
            if hasattr(x, "version_id") and x.version_id:
                ET.SubElement(obj, "VersionId").text = x.version_id
        ET.SubElement(root, "Quiet").text = "false" if verbose else "true"
        xml = ET.tostring(root, encoding="unicode", xml_declaration=True)

        # xml = (
        #     f'<?xml version="1.0" encoding="UTF-8"?>'
        #     f'<Delete xmlns="{XMLNS}">'
        #     f' {"".join(f"<Object><Key>{to_key(k)}</Key></Object>" for k in files)}'
        #     f'</Delete>'
        # )
        headers = {"content-type": "text/xml"}
        r = await self._aws_client.request(
            method="POST",
            params=dict(delete=1),
            data=xml.encode(),
            headers=headers,
            retry=self.retry,
        )
        
        root = ET.fromstring(XMLNS_RE.sub(b'', r.content))
        res = []
        for el in root:
            if el.tag == "Error":
                x = S3Error.parse_obj({v.tag: v.text for v in el})
            elif el.tag == "Deleted":
                x = S3DeletedObject.parse_obj({v.tag: v.text for v in el})
            else:
                raise RuntimeError(f'unexpected child in DeleteResult: {x.tag}')
            res.append(x)

        return res


    def signed_download_url(self, path: str, version: Optional[str] = None, max_age: int = 30) -> str:
        """
        Sign a path to authenticate download.

        The url is valid for between max_age seconds and max_age + expiry_rounding seconds.

        https://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html#RESTAuthenticationQueryStringAuth
        """
        assert not path.startswith('/'), 'path should not start with /'
        url = URL(f'{self._aws_client.endpoint}/{path}')
        url = self._aws_client.add_signed_download_params('GET', url, max_age)
        if version:
            url = url.copy_add_param('v', version)
        return str(url)


    def signed_upload_url(
        self,
        *,
        path: str,
        filename: str,
        content_type: str,
        size: int,
        content_disp: bool = True,
        expires: Optional[datetime] = None,
    ) -> Dict[str, Any]:
        """
        https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-post-example.html
        """
        assert path == '' or path.endswith('/'), 'path must be empty or end with "/"'
        assert not path.startswith('/'), 'path must not start with "/"'
        key = path + filename
        policy_conditions = [
            {'bucket': self._config.aws_s3_bucket},
            {'key': key},
            {'content-type': content_type},
            ['content-length-range', size, size],
        ]

        content_disp_fields = {}
        if content_disp:
            content_disp_fields = {'Content-Disposition': f'attachment; filename="{filename}"'}
            policy_conditions.append(content_disp_fields)

        now = utcnow()
        policy_conditions += self._aws_client.upload_extra_conditions(now)

        policy = {
            'expiration': f'{expires or now + timedelta(seconds=60):%Y-%m-%dT%H:%M:%SZ}',
            'conditions': policy_conditions,
        }
        b64_policy = base64.b64encode(json.dumps(policy).encode()).decode()

        fields = {
            'Key': key,
            'Content-Type': content_type,
            **content_disp_fields,
            'Policy': b64_policy,
            **self._aws_client.signed_upload_fields(now, b64_policy),
        }
        return dict(url=f'{self._aws_client.endpoint}/', fields=fields)


def to_key(sf: Union[S3Object, str]) -> str:
    if isinstance(sf, str):
        return sf
    elif isinstance(sf, S3Object):
        return sf.key
    else:
        raise TypeError('must be a string or S3Object object')
