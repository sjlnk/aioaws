from httpx import Response
from contextlib import suppress


class RequestError(RuntimeError):


    def __init__(self, r: Response):
        error_msg = f'unexpected response from {r.request.method} "{r.request.url}": {r.status_code}'
        super().__init__(error_msg)
        self.response = r
        self.status = r.status_code


    def __str__(self) -> str:
        s = f'{self.args[0]}'
        # not possible to get this if response is not read yet
        with suppress(Exception):
            s += f', response:\n{self.response.text}'
        return s
